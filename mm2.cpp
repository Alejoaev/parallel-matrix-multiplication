//Segunda version multiplicacion de matrices (un solo vector)
#include "matrix.hh"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;

// multiplicacion A*B y el resultado se guarda en C
void Happymult(const Matrix &A, const Matrix &B, Matrix &C)
{
    // se realiza la multiplicacion "rara"
    for (size_t i = 0; i < A.numRows(); i++)
    {
        for (size_t j = 0; j < A.numRows(); j++)
        {
            for (size_t k = 0; k < A.numRows(); k++)
            {
                if (A.at(i, k) == inf || B.at(k, j) == inf) C.at(i, j) = min(C.at(i, j),inf);
                else C.at(i, j) = min(C.at(i, j), A.at(i, k)+B.at(k, j));
            }
        }
    }
}
// Funcion extraña para multiplicar A^n
/*void strange(const Matrix &A, Matrix &C)
{
    Matrix J(A.numRows(), A.numCols());
    C = A;
    for (int i = 0; i < A.numRows(); i++)
    {
        Happymult(A, C, J);
        cout << "hola" << i << '\n';
        J.print();

        C = J;
    }
}
*/
// Funcion extraña para multiplicar A^(a cualquier exponente)
void strangeN(const Matrix &A, Matrix &B, int n)
{
    Matrix C(A.numRows(), A.numCols());
    B = A;
    for (int i = 0; i < n; i++)
    {
        Happymult(A, B, C);
        B = C;
    }
}

// Funcion extraña2 para multiplicar A^n complejidad logaritmica
void strange2(const Matrix &A, Matrix &C)
{

    if (A.numRows() == 0)
    {
        //identidad
        C.identidad();
    }
    else
    {
        if (A.numRows() == 1)
        {
            // A
            C = A;
        }
        else
        {
            if (A.numRows() % 2 == 0)
            {
                // (A*A)^(n/2)
                Matrix J(A.numRows(), A.numCols());
                Happymult(A, A, J);
                strangeN(J, C, A.numRows() / 2);
            }
            else
            {
                // A*(A*A)^((n-1)/2)
                Matrix J(A.numRows(), A.numCols());
                Matrix K(A.numRows(), A.numCols());
                Happymult(A, A, J);
                strangeN(J, K, (A.numRows() - 1) / 2);
                Happymult(A, K, C);
            }
        }
    }
}
void readFile(string fileName,Matrix& C) {
    
    string line;
    char str[1024];
    char * pch;

    int datos[3];
    int i, cont = 0;


	ifstream infile;
	infile.open (fileName);
        while(!infile.eof()) 
        {
	        getline(infile,line); 
            strcpy(str,line.c_str());

              pch = strtok (str," ");
             while (pch != NULL)
                 {
                i = atoi(pch);
                datos[cont] = i;
                pch = strtok (NULL, " ");
                cont++;
                if (cont == 3) cont = 0;
             }

          C.at(datos[0],datos[1])=datos[2];
        }

	infile.close();
}

int main()
{
    string archivo = "testg.txt";
    cout<<"Ingrese el número de nodos: ";
    int n;
    cin>>n;
    Matrix A(n, n);
    A.print();
    readFile(archivo,A);
    Matrix B(n, n);
    //A.prueba1();
    cout << '\n';
    //A.print();
    cout << '\n';
    //time start
    clock_t t_ini, t_fin;
    t_ini = clock();
    double secs;

    strange2(A,B);
    B.print();
    cout << B.Diametro();
    t_fin = clock();
    secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
    printf("%.16g milisegundos\n", secs * 1000.0);
    //time end
}